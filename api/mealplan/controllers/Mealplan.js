'use strict';

/**
 * Mealplan.js controller
 *
 * @description: A set of functions called "actions" for managing `Mealplan`.
 */

module.exports = {

  /**
   * Retrieve mealplan records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.mealplan.search(ctx.query);
    } else {
      return strapi.services.mealplan.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a mealplan record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.mealplan.fetch(ctx.params);
  },

  /**
   * Count mealplan records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.mealplan.count(ctx.query);
  },

  /**
   * Create a/an mealplan record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.mealplan.add(ctx.request.body);
  },

  /**
   * Update a/an mealplan record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.mealplan.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an mealplan record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.mealplan.remove(ctx.params);
  },

  /**
   * Add relation to a/an mealplan record.
   *
   * @return {Object}
   */

  createRelation: async (ctx, next) => {
    return strapi.services.mealplan.addRelation(ctx.params, ctx.request.body);
  },

  /**
   * Update relation to a/an mealplan record.
   *
   * @return {Object}
   */

  updateRelation: async (ctx, next) => {
    return strapi.services.mealplan.editRelation(ctx.params, ctx.request.body);
  },

  /**
   * Destroy relation to a/an mealplan record.
   *
   * @return {Object}
   */

  destroyRelation: async (ctx, next) => {
    return strapi.services.mealplan.removeRelation(ctx.params, ctx.request.body);
  }
};
